define('plugin/inbox/inbox', [
    'jquery',
    'aui',
    'underscore',
    'util/events',
    'util/navbuilder',
    'util/ajax',
    'model/page-state',
    'exports'
], function (
    $,
    AJS,
    _,
    events,
    navBuilder,
    ajax,
    pageState,
    exports
) {
    var dialogInitialised = false,
        inlineDialog,
        $inboxTrigger;

    function getInboxResourceUrlBuilder(role) {
        return navBuilder.newBuilder().pushComponents('rest', 'inbox', 'latest', 'pull-requests')
            .addParams({role: role})
            .makeBuilder();
    }

    function getInboxCountResourceUrl() {
        return navBuilder.newBuilder().pushComponents('rest', 'inbox', 'latest', 'pull-requests', 'count').makeBuilder().build();
    }

    var hideOnEscapeKeyUp = function(e) {
        if(e.keyCode === $.ui.keyCode.ESCAPE) {
            inlineDialog.hide();
            e.preventDefault();
        }
    };

    var handleInboxError = function($content, response) {
        var defaultError = {
            title: stash_i18n('stash.plugin.inbox.error.title', 'Could not retrieve inbox'),
            message: stash_i18n('stash.plugin.inbox.error.unknown', 'An unknown error occurred')
        };

        var responseError = {};
        if (response) {
            responseError = response.errors ?
                response.errors[0] :
                response;
        }
        var error = $.extend({}, defaultError, responseError);

        $content.html($(stash.widget.focusMessage.error({
            title: error.title,
            text: error.message,
            extraClasses: 'communication-error'
        })));
        return false;
    };

    function initialiseDialog($container) {
        initialiseDialogWithRoles($container, [
            {id: 'reviewer', title: stash_i18n('stash.plugin.inbox.tabs.reviewer', 'Reviewing')},
            {id: 'author', title: stash_i18n('stash.plugin.inbox.tabs.author', 'Created')}
        ]);
    }

    function initialiseDialogWithRoles($container, roles) {
        var AvatarList = require('widget/avatar-list');
        var PullRequestsTable = require('feature/pull-request/pull-request-table');
        var $dialogContents = $(stash.plugin.inbox.dialogContents({roles: roles}));

        $container.append($dialogContents);

        var handleError = function(xhr, textStatus, errorThrown, resp) {
            return handleInboxError.call(this, $container, resp);
        };

        function initPullRequestTable(role, showOnLoad) {
            var pullRequestTable = new PullRequestsTable('open', null, _.partial(getInboxResourceUrlBuilder, role), {
                'scope': 'global',
                'target': '#inbox-pull-request-table-' + role,
                'scrollPaneSelector': '.inbox-table-wrapper',
                'bufferPixels': 50,
                'pageSize': 10,
                'spinnerSize': 'medium',
                'noneFoundMessageHtml': stash.plugin.inbox.emptyInboxMessage(),
                'dataLoadedEvent': 'stash.plugin.inbox.dataLoaded.' + role,
                'statusCode': {
                    0: handleError,
                    401: handleError,
                    500: handleError,
                    502: handleError
                }
            });
            pullRequestTable.handleErrors = $.noop; // we handle the errors ourselves
            pullRequestTable.init();

            if (showOnLoad) {
                AJS.tabs.change($container.find('[href=#inbox-pull-request-' + role + ']'));
            }
        }

        _.each(roles, function(role, i) {
            initPullRequestTable(role.id, i === 0);
        });

        // Unfortunately AJS.tabs.setup() is called before we get a chance to create the HTML, need to do this manually
        $container.find('.tabs-menu').delegate("a", "click", function (e) {
            AJS.tabs.change(AJS.$(this), e);
            e && e.preventDefault();
        });
        AvatarList.init();
    }

    var onShowDialog = function ($content, trigger, showPopup) {
        showPopup();
        $(document).on('keyup', hideOnEscapeKeyUp);

        if (!dialogInitialised) {
            var $spinner = $('<div class="loading-resource-spinner"></div>');
            $content.html($spinner);
            $spinner.show().spin('medium');

            WRM.require('wrc!stash.pullRequest.inbox').always(function() {
                $spinner.spinStop().remove();
            }).done(function() {
                initialiseDialog($content);
                dialogInitialised = true;
            });
        }
    };

    var onHideDialog = function () {
        $(document).off('keyup', hideOnEscapeKeyUp);
        if ($(document.activeElement).closest('#inbox-pull-requests-content').length) {
            // if the focus is inside the dialog, you get stuck when it closes.
            document.activeElement.blur();
        }
    };

    var fetchInboxCount = function() {
        ajax.rest({
            url: getInboxCountResourceUrl(),
            type: 'GET',
            statusCode: {
                '*': function () {
                    return false;
                }
            }
        }).done(function (data) {
            if (data.count > 0) {
                var $badge = $(aui.badges.badge({
                    'text': data.count
                }));
                $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: false}))
                    .append($badge);
                setTimeout(function () {
                    $badge.addClass('visible');
                }, 200);
            } else {
                // The badge fadeOut transition happens with a CSS3 transition, which we can't hook into.
                // Use a setTimeout instead, unfortunately.
                var cssTransitionDuration = 500;
                $inboxTrigger.find(".aui-badge").removeClass('visible');
                setTimeout(function () {
                    $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: true}));
                }, cssTransitionDuration);
            }
        });
    };

    exports.onReady = function () {
        $inboxTrigger = $("#inbox-pull-requests");
        if ($inboxTrigger.length && pageState.getCurrentUser()) {
            $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: true}));
            inlineDialog = AJS.InlineDialog($inboxTrigger, 'inbox-pull-requests-content', onShowDialog, {
                width: 840,
                hideCallback: onHideDialog
            });

            fetchInboxCount();

            var _approvalHandler = function(data) {
                if (data.user.name === pageState.getCurrentUser().name) {
                    fetchInboxCount();
                }
            };

            events.on('stash.widget.approve-button.added', _approvalHandler);
            events.on('stash.widget.approve-button.removed', _approvalHandler);
        }
    };
});

AJS.$(document).ready(function () {
    require('plugin/inbox/inbox').onReady();
});
